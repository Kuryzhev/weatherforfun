'use strict';

define([
    'angular',
    'angularUIRouter',
    'global/global.module',
    'components/google-map/google-map.module'
], function (angular, angularUIRouter) {
    angular.module('wff', [
        'ui.router',
        'wff.global',
        'wff.googleMap'
    ]).config(['$stateProvider', '$urlRouterProvider', "$locationProvider",
        function ($stateProvider, $urlRouterProvider, $locationProvider) {
            $stateProvider
                .state('global', {
                    url: '/',
                    templateUrl:"app/global/global-view.html",
                    controller: "globalController",
                })
            $locationProvider.html5Mode(true);
            $urlRouterProvider.otherwise('/');
        }])
});

