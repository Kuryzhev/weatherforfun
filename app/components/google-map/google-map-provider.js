/**
 * Created by ����� on 24.08.2015.
 */
define([], function () {
    return [function () {
        var map, lastCenter, mapBounds, currentZoom, isTopLimit;
        var latsForZoom = {
            '2': 50,
            '3': 75.5,
            '4': 79.9,
            '5': 82,
            '6': 83.9,
            '7': 84.5,
            '8': 85,
            '9': 85.4,
            '10': 85.6,
            '11': 85.7,
            '12': 85.8
        };
        return {
            $get: ['$timeout', function ($timeout) {
                return {
                    getMainMap: function () {
                        return map;
                    },
                    initializeMap: function () {
                        try {
                            map = new google.maps.Map(arguments[0], arguments[1]);
                            map.checkBound = function () {
                                //TODO: Find another way to dodge infinite event popping.
                                $timeout(function () {
                                    currentZoom = map.getZoom();
                                    mapBounds = map.getBounds();
                                    isTopLimit = mapBounds.getNorthEast().lat() > 85;
                                    if (mapBounds.getSouthWest().lat() < -85 || isTopLimit) {
                                        map.setCenter(new google.maps.LatLng(isTopLimit ? latsForZoom[currentZoom] : latsForZoom[currentZoom] * (-1), lastCenter.K))
                                    } else
                                        lastCenter = map.getCenter()
                                }, 0)
                            };
                            lastCenter = map.getCenter()
                            google.maps.event.addListener(map, 'center_changed', map.checkBound);
                            google.maps.event.addListener(map, 'zoom_changed', map.checkBound);
                        }
                        catch (e) {
                            throw ('failed to initialize google maps');
                        }

                    }
                }
            }]
        }
    }]
});
