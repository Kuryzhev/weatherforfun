define([
    'angular',
    'components/google-map/google-map-provider',
    'components/google-map/wff-google-map'
], function (angular,googleMapProvider,googleMapDirective) {
    angular.module('wff.googleMap', [])
        .provider('$googleMap',googleMapProvider)
        .directive('wffGoogleMap',googleMapDirective)
});

