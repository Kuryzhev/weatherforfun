define([], function () {
    return ['$googleMap', function ($googleMap) {
        return {
            restrict: 'E',
            scope: {},
            link: function (scope, element, attrs) {
                var script = document.createElement("script");
                script.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyDntCgBpefqt5qsmQ0A9Le2Purf2Y_bV0Y&callback=initMap";    // use this for linked script
                document.body.appendChild(script);
            },
            controller: ['$scope', '$window', function ($scope, $window) {
                $window.initMap = function () {
                    $googleMap.initializeMap(document.getElementById('map-wrapper'), {
                        center: {lat: 51.5, lng: 15.6},
                        zoom: 5,
                        maxZoom:12,
                        minZoom:2
                    });
                    $scope.$digest();
                };

                $scope.$watch($googleMap.getMainMap, function (newVal) {
                    $scope.mainMap = newVal;
                    console.log(newVal);
                })
            }],
            template: '\
                <div id="map-wrapper">\
                    \
                </div>\
                '
        }
    }]
});
