define([
    'angular',
    'global/global-controller'
], function (angular,globalController) {
    angular.module('wff.global', [])
        .controller('globalController',globalController)
});
