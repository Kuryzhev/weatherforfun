/**
 * Created by user on 20.08.2015.
 */
'use strict';

define(['app', 'angularMocks'], function (app) {
    describe('wff.global.module', function () {
        beforeEach(module('wff.global'));
        describe('global-controller', function () {
            var globalController,
                scope;
            beforeEach(inject(function ($rootScope, $controller) {
                scope = $rootScope.$new();
                globalController = $controller('globalController', {
                    $scope: scope
                });
            }));
            it('helloName should be equal to "Artem"', inject(function ($controller) {
                expect(scope.helloName).toEqual('Artem');
            }));
        });
    });
});