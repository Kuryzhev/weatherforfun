/**
 * Created by Artem on 02.07.2015.
 */
module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        //coffee: {
        //    glob_to_multiple: {
        //        expand: true,
        //        flatten: true,
        //        cwd: 'src/coffee', //из этой папки
        //        src: ['*.coffee'], // все coffee файлы
        //        dest: 'src/js', // компилируется в эту папку
        //        ext: '.js' // в js файлы
        //    }
        //
        //},
        //
        sass: {
            dist: {
                files: [{
                    expand: true,
                    dest: 'dest/css',
                    src: ['app/**/*.scss', 'app/**/**/*.scss'],
                    ext: '.css',
                    flatten: true
                }]
            }
        },
        concat: {
            css: {
                src: 'dest/css/*.css',
                dest: 'dest/main.css'
            }
        },
        requirejs: {
            compile: {
                options: {
                    mainConfigFile: "app/require-config.js", // главный файл с описанием конфигурации и билда require.js
                    baseUrl: "", // папка где находятся все js файлы
                    name: 'app/require-config', // название файла запускающего приложение
                    include: ['app/require-config'], // вставить в выходящий файл и require-config.js
                    out: "../dest/main.min.js" // выходящий минифицированный и конкатенированный файл готовые для продакшена
                }
            }
        },
        watch: {
            css: {
                files: ['app/**/*.scss','app/**/**/*.scss'], // следить за изменениями любых файлов с разширениями .scss
                tasks: ['sass','concat:css'] // и запускать такую задачу при их изменении
            }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-contrib-concat');

    grunt.registerTask('sass-default', ['sass','concat:css'])
    grunt.registerTask('default', ['sass','concat:css', 'requirejs', 'watch']);
};